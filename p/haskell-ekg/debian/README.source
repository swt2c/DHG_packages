There are a few notes about the upstream source.

First, it includes some third-party libraries in assets/:

- jquery 1.6.4
- jquery flot
- bootstrap 1.4.0

bootstrap 1.4.0 is not the version that is present in Debian (and is
incompatible with that), so we have to use the embedded copy. I still
have to determine whether we can replace the jquery libs with the ones
shipped in Debian.

Furthermore, there are (also in assets/) two icons that are from a
third-party and licensed under CC-BY-3.0 (the license for these is not
shipped in the upstream tarball, but you can see it at
https://github.com/tibbe/ekg/blob/HEAD/LICENSE.icons).

And last, we store in the VCS the minified form of the embedded
javascript libraries; these can be regenerated (with the correct
dependencies installed, i.e. node-uglify) via the "minify-js" rules
target. The reason we don't generate these at build-time is that the
minifiers are available on just a few platforms, which would restrict
this library needlessly.

 -- Iustin Pop <iustin@debian.org>, Mon, 16 Apr 2012 21:44:41 +0200
