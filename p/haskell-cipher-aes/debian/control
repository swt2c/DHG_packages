Source: haskell-cipher-aes
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders:
 Clint Adams <clint@debian.org>,
Priority: optional
Section: haskell
Rules-Requires-Root: no
Build-Depends:
 cdbs,
 debhelper (>= 10),
 ghc (>= 8),
 ghc-prof,
 haskell-devscripts (>= 0.13),
 libghc-byteable-dev,
 libghc-byteable-prof,
 libghc-crypto-cipher-tests-dev (>= 0.0.8),
 libghc-crypto-cipher-types-dev (<< 0.1),
 libghc-crypto-cipher-types-dev (>= 0.0.6),
 libghc-crypto-cipher-types-prof,
 libghc-quickcheck2-dev (>= 2),
 libghc-securemem-dev (>= 0.1.2),
 libghc-securemem-prof,
 libghc-test-framework-dev (>= 0.3.3),
 libghc-test-framework-quickcheck2-dev (>= 0.2.9),
Build-Depends-Indep:
 ghc-doc,
 libghc-byteable-doc,
 libghc-crypto-cipher-types-doc,
 libghc-securemem-doc,
Standards-Version: 4.5.0
Homepage: https://github.com/vincenthz/hs-cipher-aes
Vcs-Browser: https://salsa.debian.org/haskell-team/DHG_packages/tree/master/p/haskell-cipher-aes
Vcs-Git: https://salsa.debian.org/haskell-team/DHG_packages.git [p/haskell-cipher-aes]
X-Description: Fast AES cipher implementation
 The modes of operations available are ECB (Electronic code book),
 CBC (Cipher block chaining), CTR (Counter), XTS (XEX with ciphertext
 stealing), GCM (Galois Counter Mode).
 .
 The AES implementation uses AES-NI when available (on x86 and x86-64
 architecture), but fallback gracefully to a software C implementation.
 .
 The software implementation uses S-Boxes, which might suffer for cache
 timing issues.  However do note that most other known software
 implementations, including very popular one (openssl, gnutls) also uses
 same implementation. If it matters for your case, you should make sure
 you have AES-NI available, or you'll need to use a different
 implementation.

Package: libghc-cipher-aes-dev
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-cipher-aes-prof
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-cipher-aes-doc
Architecture: all
Section: doc
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}
