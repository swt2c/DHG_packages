Source: haskell-hstringtemplate
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders:
 Giovanni Mascellani <gio@debian.org>,
Priority: optional
Section: haskell
Rules-Requires-Root: no
Build-Depends:
 cdbs,
 debhelper (>= 10),
 ghc (>= 8.4.3),
 ghc-prof,
 haskell-devscripts (>= 0.13),
 libghc-blaze-builder-dev,
 libghc-blaze-builder-prof,
 libghc-old-locale-dev,
 libghc-old-locale-prof,
 libghc-semigroups-dev (>= 0.16),
 libghc-semigroups-prof,
 libghc-syb-dev,
 libghc-syb-prof,
 libghc-void-dev,
 libghc-void-prof,
Build-Depends-Indep:
 ghc-doc,
 libghc-blaze-builder-doc,
 libghc-old-locale-doc,
 libghc-semigroups-doc,
 libghc-syb-doc,
 libghc-void-doc,
Standards-Version: 4.5.0
Homepage: https://hackage.haskell.org/package/HStringTemplate
Vcs-Browser: https://salsa.debian.org/haskell-team/DHG_packages/tree/master/p/haskell-hstringtemplate
Vcs-Git: https://salsa.debian.org/haskell-team/DHG_packages.git [p/haskell-hstringtemplate]

Package: libghc-hstringtemplate-dev
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: Haskell port of the Java library StringTemplate${haskell:ShortBlurb}
 This is a Haskell template engine. Its API is mainly inspired to that of
 the Java library StringTemplate, but it is implemented in a "Haskellish"
 manner.
 .
 It implements the basic 3.1 grammar, lacking group files (though not
 groups themselves), Regions, and Interfaces and extends it by allowing
 the application of alternating attributes to anonymous as well as
 regular templates, including therefore sets of alternating attributes.
 .
 ${haskell:Blurb}

Package: libghc-hstringtemplate-prof
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: Haskell port of the Java library StringTemplate${haskell:ShortBlurb}
 This is a Haskell template engine. Its API is mainly inspired to that of
 the Java library StringTemplate, but it is implemented in a "Haskellish"
 manner.
 .
 It implements the basic 3.1 grammar, lacking group files (though not
 groups themselves), Regions, and Interfaces and extends it by allowing
 the application of alternating attributes to anonymous as well as
 regular templates, including therefore sets of alternating attributes.
 .
 ${haskell:Blurb}

Package: libghc-hstringtemplate-doc
Architecture: all
Section: doc
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Description: Haskell port of the Java library StringTemplate${haskell:ShortBlurb}
 This is a Haskell template engine. Its API is mainly inspired to that of
 the Java library StringTemplate, but it is implemented in a "Haskellish"
 manner.
 .
 It implements the basic 3.1 grammar, lacking group files (though not
 groups themselves), Regions, and Interfaces and extends it by allowing
 the application of alternating attributes to anonymous as well as
 regular templates, including therefore sets of alternating attributes.
 .
 ${haskell:Blurb}
