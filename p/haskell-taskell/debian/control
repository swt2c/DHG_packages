Source: haskell-taskell
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders: Clint Adams <clint@debian.org>
Priority: optional
Section: haskell
Build-Depends: debhelper (>= 10),
 haskell-devscripts-minimal | haskell-devscripts (>= 0.13),
 cdbs,
 ghc,
 ghc-prof,
 libghc-aeson-dev (>= 1.4.2.0),
 libghc-aeson-dev (<< 1.5),
 libghc-aeson-prof (>= 1.4.2.0),
 libghc-aeson-prof (<< 1.5),
 libghc-attoparsec-dev (>= 0.13.2.2),
 libghc-attoparsec-dev (<< 0.14),
 libghc-attoparsec-prof (>= 0.13.2.2),
 libghc-attoparsec-prof (<< 0.14),
 libghc-brick-dev (>= 0.47),
 libghc-brick-dev (<< 0.48),
 libghc-brick-prof (>= 0.47),
 libghc-brick-prof (<< 0.48),
 libghc-classy-prelude-dev (>= 1.5.0),
 libghc-classy-prelude-dev (<< 1.6),
 libghc-classy-prelude-prof (>= 1.5.0),
 libghc-classy-prelude-prof (<< 1.6),
 libghc-config-ini-dev (>= 0.2.4.0),
 libghc-config-ini-dev (<< 0.3),
 libghc-config-ini-prof (>= 0.2.4.0),
 libghc-config-ini-prof (<< 0.3),
 libghc-file-embed-dev (>= 0.0.11),
 libghc-file-embed-dev (<< 0.1),
 libghc-file-embed-prof (>= 0.0.11),
 libghc-file-embed-prof (<< 0.1),
 libghc-fold-debounce-dev (>= 0.2.0.8),
 libghc-fold-debounce-dev (<< 0.3),
 libghc-fold-debounce-prof (>= 0.2.0.8),
 libghc-fold-debounce-prof (<< 0.3),
 libghc-http-client-dev (>= 0.5.14),
 libghc-http-client-dev (<< 0.6),
 libghc-http-client-prof (>= 0.5.14),
 libghc-http-client-prof (<< 0.6),
 libghc-http-conduit-dev (>= 2.3.6.1),
 libghc-http-conduit-dev (<< 2.4),
 libghc-http-conduit-prof (>= 2.3.6.1),
 libghc-http-conduit-prof (<< 2.4),
 libghc-http-types-dev (>= 0.12.3),
 libghc-http-types-dev (<< 0.13),
 libghc-http-types-prof (>= 0.12.3),
 libghc-http-types-prof (<< 0.13),
 libghc-lens-dev (>= 4.17),
 libghc-lens-dev (<< 4.18),
 libghc-lens-prof (>= 4.17),
 libghc-lens-prof (<< 4.18),
 libghc-vty-dev (>= 5.25.1),
 libghc-vty-dev (<< 5.26),
 libghc-vty-prof (>= 5.25.1),
 libghc-vty-prof (<< 5.26),
 libghc-raw-strings-qq-dev (>= 1.1),
 libghc-raw-strings-qq-dev (<< 1.2),
 libghc-raw-strings-qq-prof (>= 1.1),
 libghc-raw-strings-qq-prof (<< 1.2),
 libghc-tasty-dev (>= 1.2),
 libghc-tasty-dev (<< 1.3),
 libghc-tasty-prof (>= 1.2),
 libghc-tasty-prof (<< 1.3),
 libghc-tasty-discover-dev (>= 4.2.1),
 libghc-tasty-discover-dev (<< 4.3),
 libghc-tasty-discover-prof (>= 4.2.1),
 libghc-tasty-discover-prof (<< 4.3),
 libghc-tasty-expected-failure-dev (>= 0.11.1.1),
 libghc-tasty-expected-failure-dev (<< 0.12),
 libghc-tasty-expected-failure-prof (>= 0.11.1.1),
 libghc-tasty-expected-failure-prof (<< 0.12),
 libghc-tasty-hunit-dev (>= 0.10.0.1),
 libghc-tasty-hunit-dev (<< 0.11),
 libghc-tasty-hunit-prof (>= 0.10.0.1),
 libghc-tasty-hunit-prof (<< 0.11),
 tasty-discover <!nocheck>,
Build-Depends-Indep: ghc-doc,
 libghc-aeson-doc,
 libghc-attoparsec-doc,
 libghc-brick-doc,
 libghc-classy-prelude-doc,
 libghc-config-ini-doc,
 libghc-file-embed-doc,
 libghc-fold-debounce-doc,
 libghc-http-client-doc,
 libghc-http-conduit-doc,
 libghc-http-types-doc,
 libghc-lens-doc,
 libghc-vty-doc,
Standards-Version: 4.5.0
Homepage: https://github.com/smallhadroncollider/taskell#readme
X-Description: command-line Kanban board/task manager
 Taskell is a CLI kanban board/task manager featuring
  - Per project task lists
  - vim style key-bindings
  - Stored using Markdown
  - Clean diffs for easy version control
  - Support for sub-tasks and due dates
  - Trello board imports
  - GitHub project imports

Package: libghc-taskell-dev
Architecture: any
Depends: ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends: ${haskell:Recommends},
Suggests: ${haskell:Suggests},
Conflicts: ${haskell:Conflicts},
Provides: ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-taskell-prof
Architecture: any
Depends: ${haskell:Depends},
 ${misc:Depends},
Recommends: ${haskell:Recommends},
Suggests: ${haskell:Suggests},
Conflicts: ${haskell:Conflicts},
Provides: ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-taskell-doc
Architecture: all
Section: doc
Depends: ${haskell:Depends},
 ${misc:Depends},
Recommends: ${haskell:Recommends},
Suggests: ${haskell:Suggests},
Conflicts: ${haskell:Conflicts},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: taskell
Architecture: any
Section: misc
Depends: ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends: ${haskell:Recommends},
Suggests: ${haskell:Suggests},
Conflicts: ${haskell:Conflicts},
Provides: ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}
