Source: haskell-sdl-image
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders:
 Miriam Ruiz <little_miry@yahoo.es>,
Priority: optional
Section: haskell
Rules-Requires-Root: no
Build-Depends:
 alex,
 c2hs,
 cdbs,
 cpphs,
 debhelper (>= 10),
 ghc (>= 8),
 ghc-prof,
 haskell-devscripts (>= 0.13),
 libghc-sdl-dev,
 libghc-sdl-prof,
 libgl1-mesa-dev | libgl-dev,
 libglu1-mesa-dev | libglu-dev,
 libsdl-gfx1.2-dev,
 libsdl-image1.2-dev,
 libsdl1.2-dev,
 libx11-dev,
 pkg-config,
Build-Depends-Indep: ghc-doc,
 libghc-sdl-doc,
Standards-Version: 4.5.0
Homepage: https://hackage.haskell.org/package/SDL-image
Vcs-Browser: https://salsa.debian.org/haskell-team/DHG_packages/tree/master/p/haskell-sdl-image
Vcs-Git: https://salsa.debian.org/haskell-team/DHG_packages.git [p/haskell-sdl-image]

Package: libghc-sdl-image-dev
Architecture: any
Depends:
 libsdl-gfx1.2-dev,
 libsdl-image1.2-dev,
 libsdl1.2-dev,
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Description: Haskell SDL Image binding for GHC
 This package provides the SDL Image library bindings for the Haskell
 programming language. SDL Image is a simple library to load images of
 various formats as SDL surfaces.

Package: libghc-sdl-image-prof
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Description: Haskell SDL Image binding for GHC - profiling libraries
 This package provides the SDL Image library bindings for the Haskell
 programming language, compiled for profiling. SDL Image is a simple
 library to load images of various formats as SDL surfaces.

Package: libghc-sdl-image-doc
Architecture: all
Section: doc
Depends:
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Description: Haskell SDL Image binding for GHC - documentation
 This package provides the documentation for the SDL Image library bindings
 for the Haskell programming language. SDL Image is a simple
 library to load images of various formats as SDL surfaces.
