haskell-diagrams-gtk (1.4-7) unstable; urgency=medium

  * Patch for newer base.  closes: #964053.

 -- Clint Adams <clint@debian.org>  Tue, 30 Jun 2020 17:41:44 -0400

haskell-diagrams-gtk (1.4-6) unstable; urgency=medium

  * Patch for newer deps

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Fri, 06 Sep 2019 20:03:30 +0200

haskell-diagrams-gtk (1.4-5) unstable; urgency=medium

  * Newer build-deps from hackage
  * Update d/copyright file
    - Add missing copyright holders
    - Set license to BSD-3-clause

 -- Ilias Tsitsimpis <iliastsi@debian.org>  Sun, 04 Nov 2018 13:54:22 +0200

haskell-diagrams-gtk (1.4-4) unstable; urgency=medium

  [ Clint Adams ]
  * Set Rules-Requires-Root to no.

  [ Ilias Tsitsimpis ]
  * Bump debhelper compat level to 10

 -- Ilias Tsitsimpis <iliastsi@debian.org>  Sun, 30 Sep 2018 21:06:34 +0300

haskell-diagrams-gtk (1.4-3) unstable; urgency=medium

  * Patch for newer base.

 -- Clint Adams <clint@debian.org>  Tue, 17 Apr 2018 01:01:44 -0400

haskell-diagrams-gtk (1.4-2) unstable; urgency=medium

  [ Ilias Tsitsimpis ]
  * Change Priority to optional. Since Debian Policy version 4.0.1,
    priority extra has been deprecated.
  * Use the HTTPS form of the copyright-format URL
  * Modify d/watch and Source field in d/copyright to use HTTPS
  * Declare compliance with Debian policy 4.1.1
  * Use salsa.debian.org URLs in Vcs-{Browser,Git} fields

  [ Clint Adams ]
  * Bump to Standards-Version 4.1.4.

 -- Clint Adams <clint@debian.org>  Mon, 09 Apr 2018 20:04:47 -0400

haskell-diagrams-gtk (1.4-1) unstable; urgency=medium

  * New upstream release

 -- Clint Adams <clint@debian.org>  Thu, 06 Jul 2017 17:07:04 -0400

haskell-diagrams-gtk (1.3.0.2-3) unstable; urgency=medium

  * Upload to unstable as part of GHC 8 transition.

 -- Clint Adams <clint@debian.org>  Thu, 27 Oct 2016 18:33:21 -0400

haskell-diagrams-gtk (1.3.0.2-2) experimental; urgency=medium

  * Temporarily build-depend on ghc 8.

 -- Clint Adams <clint@debian.org>  Mon, 17 Oct 2016 20:42:17 -0400

haskell-diagrams-gtk (1.3.0.2-1) unstable; urgency=medium

  [ Dmitry Bogatov ]
  * Use secure (https) uri in Vcs-Git field in 'debian/control'
  * Bump standards version to 3.9.8 (no changes needed)

  [ Clint Adams ]
  * New upstream release

 -- Clint Adams <clint@debian.org>  Tue, 12 Jul 2016 16:39:02 -0400

haskell-diagrams-gtk (1.3.0.1-1) unstable; urgency=medium

  * New upstream release

 -- Joachim Breitner <nomeata@debian.org>  Wed, 23 Mar 2016 09:03:03 +0100

haskell-diagrams-gtk (1.3-2) unstable; urgency=medium

  * Switch Vcs-Git/Vcs-Browser headers to new location.

 -- Clint Adams <clint@debian.org>  Thu, 03 Dec 2015 14:54:21 -0500

haskell-diagrams-gtk (1.3-1) experimental; urgency=medium

  * New upstream release
  * Bump standards-version to 3.9.6
  * Depend on haskell-devscripts >= 0.10 to ensure that this package
    builds against GHC in experimental

 -- Joachim Breitner <nomeata@debian.org>  Thu, 20 Aug 2015 10:27:27 +0200

haskell-diagrams-gtk (1.0.1.3-4) unstable; urgency=medium

  * Rebuild due to haskell-devscripts bug affecting the previous

 -- Joachim Breitner <nomeata@debian.org>  Tue, 28 Apr 2015 23:58:28 +0200

haskell-diagrams-gtk (1.0.1.3-3) unstable; urgency=medium

  * Upload to unstable

 -- Joachim Breitner <nomeata@debian.org>  Mon, 27 Apr 2015 11:49:12 +0200

haskell-diagrams-gtk (1.0.1.3-2) experimental; urgency=medium

  * Depend on haskell-devscripts 0.9, found in experimental

 -- Joachim Breitner <nomeata@debian.org>  Sat, 20 Dec 2014 17:10:16 +0100

haskell-diagrams-gtk (1.0.1.3-1) unstable; urgency=medium

  * New upstream release

 -- Joachim Breitner <nomeata@debian.org>  Mon, 22 Sep 2014 14:10:16 +0200

haskell-diagrams-gtk (1.0.1.2-1) unstable; urgency=medium

  * New upstream release

 -- Joachim Breitner <nomeata@debian.org>  Wed, 04 Jun 2014 12:48:29 +0200

haskell-diagrams-gtk (1.0.1-1) unstable; urgency=medium

  * New upstream release
  * Bump standards version, no change

 -- Raúl Benencia <rul@kalgan.cc>  Sat, 19 Apr 2014 12:00:27 -0300

haskell-diagrams-gtk (0.6.0.1-1) unstable; urgency=low

  * Adjust watch file to new hackage layout
  * New upstream release

 -- Joachim Breitner <nomeata@debian.org>  Sun, 24 Nov 2013 14:11:44 +0000

haskell-diagrams-gtk (0.6-2) unstable; urgency=low

  * Enable compat level 9
  * Bump standards version to 3.9.4
  * Use substvars for Haskell description blurbs

 -- Joachim Breitner <nomeata@debian.org>  Fri, 24 May 2013 12:50:26 +0200

haskell-diagrams-gtk (0.6-1) experimental; urgency=low

  * Initial release.

 -- Joachim Breitner <nomeata@debian.org>  Sat, 05 Jan 2013 13:14:12 +0100
